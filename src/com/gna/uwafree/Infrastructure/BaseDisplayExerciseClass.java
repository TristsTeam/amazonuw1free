package com.gna.uwafree.Infrastructure;

import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.gna.uwafree.R;

public class BaseDisplayExerciseClass extends CommonBaseClass {
	protected String[] exerciseTextArray;
	protected String[] exerciseNameArray;
	protected int[] firstPictureArray;
	protected int[] secondPictureArray;
	protected int[] thirdPictureArray;
	protected int[] imageCount;

	protected ImageView firstImage;
	protected ImageView secondImage;
	protected ImageButton rightArrowBtn;
	protected ImageButton leftArrowBtn;
	protected ImageButton cancelBtn;
	protected TextView exerciseText;
	protected TextView exerciseCount;
	protected TextView exerciseName;
	protected ImageView titleView;
	protected ImageButton infoBtn;

	public void intializeViews() {
		firstImage = (ImageView) findViewById(R.id.firstImage);
		secondImage = (ImageView) findViewById(R.id.secondImage);
		rightArrowBtn = (ImageButton) findViewById(R.id.rightArrowBtn);
		leftArrowBtn = (ImageButton) findViewById(R.id.leftArrowBtn);
		exerciseText = (TextView) findViewById(R.id.exerciseText);
		exerciseCount = (TextView) findViewById(R.id.exerciseCount);
		exerciseName = (TextView) findViewById(R.id.exerciseName);
		titleView = (ImageView) findViewById(R.id.titleView);
		cancelBtn = (ImageButton) findViewById(R.id.cancelBtn);
		infoBtn = (ImageButton) findViewById(R.id.infoBtn);

	}

	public void workoutAExercise() {
		titleView.setBackgroundResource(R.drawable.title01);
		exerciseTextArray = new String[7];
		exerciseNameArray = new String[7];
		firstPictureArray = new int[7];
		secondPictureArray = new int[7];
		thirdPictureArray = new int[7];
		imageCount = new int[7];

		exerciseTextArray[0] = "With feet on an elevated surface, palms shoulder width apart, perform a push up.Keep your core tight.";
		exerciseTextArray[1] = "Holding two dumbbells in a �hammer grip�, lunge forward on your left leg, as you curl both weights upward. Pause, control balance, return and repeat on the other side.";
		exerciseTextArray[2] = "Keeping two dumbbells at a 90 degree shoulder position, begin to perform a jumping jack as you press both weights upward. Continue this motion.  Do not lower weights below 90 degree arm position.  Tense your core.";
		exerciseTextArray[3] = "Standing, with two dumbbells, tense your core and begin pressing the weight upward, return then repeat the other side.";
		exerciseTextArray[4] = "Holding heavier Dumbbells, bend forward, keeping your back in a controlled braced position, squat/drop downward, then extend your body, straighten legs and thrust back upward, with heels digging into the floor. Pause and repeat.";
		exerciseTextArray[5] = "Laying on your back, hold two dumbbells up with arms straight, then moving only your forearms, lower weight in a hammer grip to the side of your head, pause then return to start. (for advanced, move elbows further toward head to increase continual tension on the tricep.";
		exerciseTextArray[6] = "Holding two dumbbells in a push up position, brace your core so you do not dip or arch your hips, begin to row alternately each dumbbell to your side, pausing then return and repeat.";

		exerciseNameArray[0] = "Elevated Push Up";
		exerciseNameArray[1] = "Lung Hammer Curl";
		exerciseNameArray[2] = "DB Jumping Jack";
		exerciseNameArray[3] = "Alt Shoulder Press";
		exerciseNameArray[4] = "DB Deadlift";
		exerciseNameArray[5] = "DB SkullKrusher [Floor]";
		exerciseNameArray[6] = "Renegade Rows";

		firstPictureArray[0] = R.drawable.w1_exercise01_01;
		firstPictureArray[1] = R.drawable.w1_exercise02_01;
		firstPictureArray[2] = R.drawable.w1_exercise03_01;
		firstPictureArray[3] = R.drawable.w1_exercise04_01;
		firstPictureArray[4] = R.drawable.w1_exercise05_01;
		firstPictureArray[5] = R.drawable.w1_exercise06_01;
		firstPictureArray[6] = R.drawable.w1_exercise07_01;

		secondPictureArray[0] = R.drawable.w1_exercise01_02;
		secondPictureArray[1] = R.drawable.w1_exercise02_02;
		secondPictureArray[2] = R.drawable.w1_exercise03_02;
		secondPictureArray[3] = R.drawable.w1_exercise04_02;
		secondPictureArray[4] = R.drawable.w1_exercise05_02;
		secondPictureArray[5] = R.drawable.w1_exercise06_02;
		secondPictureArray[6] = R.drawable.w1_exercise07_02;

		thirdPictureArray[0] = 0;
		thirdPictureArray[1] = 0;
		thirdPictureArray[2] = 0;
		thirdPictureArray[3] = 0;
		thirdPictureArray[4] = 0;
		thirdPictureArray[5] = 0;
		thirdPictureArray[6] = 0;

		imageCount[0] = 2;
		imageCount[1] = 2;
		imageCount[2] = 2;
		imageCount[3] = 2;
		imageCount[4] = 2;
		imageCount[5] = 2;
		imageCount[6] = 2;

	}

}
